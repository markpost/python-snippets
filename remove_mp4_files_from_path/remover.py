#!/usr/bin/env python
"""
This class is the main class for the remover of mp4-files from the given path.
The class is called by the method process the path (plus subdirectories)to
delete mp4-files of. When process is called the path is retrieved from the
arguments and validated whether the path exists and is a directory. The user is
asked for a confirmation before deletion of all mp4-files.
"""

import logging
import os

from helper import Helper


class Remover:

    def __init__(self):
        pass

    def process(self, arguments):
        """
        The entry point of the class. The method will process the deletion after
        validation of the path argument and user confirmation.
        :param arguments: The arguments given when running the application
        """

        if Helper.is_path_given(arguments):
            path = arguments[1]

            if Remover.confirm_with_user(path):
                logging.info("Start removing mp4-files from path: " + path)

                total_files, removed_files, removed_size = self.remove_files_from_path(
                    path)

                removed_size_megabytes, removed_size_mebibytes = \
                    self.convert_bytes_to_mega_bytes(removed_size)

                logging.info(
                    "Finished removing mp4-files from path: " + path + "\n")
                logging.info("Total files scanned: " + str(total_files))
                logging.info("Removed mp4-files: " + str(removed_files))
                logging.info("Total size removed files: " + str(
                    removed_size_megabytes) + "MB - " + str(
                    removed_size_mebibytes) + "MiB")

    @staticmethod
    def is_path_given(arguments) -> bool:
        """
        The method checks whether the arguments list contains a path and whether
        this given path is valid
        :param arguments: The arguments list to check
        :return: Whether the arguments contained a path and whether the path is
        valid
        """

        if 2 > len(arguments):
            logging.error("No path given in the arguments. Please give a path "
                           + "and run again.")
            return False

        path = arguments[1]
        path = os.path.normpath(path)

        if not Helper.is_path_dir(path):
            logging.error("Given path is invalid or not a path of a directory.")
            return False

        return True

    @staticmethod
    def confirm_with_user(path) -> bool:
        """
        The method asks the user for confirmation whether the user is sure that it
        wants to delete all mp4-files from the given path and subdirectories.
        :param path: The path to delete the mp4-files from
        :return: The confirmation of the user
        """

        text = ""

        while text is not "Y" and text is not "N":
            text = input(
                "Are you sure you want to delete all mp4-files from \"" +
                path + "\" and subdirectories? Y/N ")

        if text == "Y":
            return True
        else:
            return False

    def remove_files_from_path(self, path) -> (int, int, int):
        """
        The method loops over all directories, subdirectories and files of the
        given path.
        :param path: The path to process
        :return: The total number of files scanned, the number of deleted mp4-files
        and the total size of the removed files
        """

        total_files = 0
        total_mp4_files = 0
        total_size_mp4_files = 0

        for current_path, dirs, files in os.walk(path):
            for file_name in files:
                total_files += 1
                file_path = current_path + "\\" + file_name

                mp4_file, size_mp4_file = self.process_file(file_path)

                total_mp4_files += mp4_file
                total_size_mp4_files += size_mp4_file

        return total_files, total_mp4_files, total_size_mp4_files

    @staticmethod
    def process_file(file_path) -> (int, int):
        """
        The method processes the input file path. It checks whether the given file
        is a mp4-file or not
        :param file_path: The path of the file to process
        :return: 1 if mp4-file or 0 if not, size whether mp4-file or 0 if not
        """

        mp4_file = 0
        size_mp4_file = 0

        if Helper.is_file_mp4(file_path):
            mp4_file = 1
            size_mp4_file = Helper.get_file_size(file_path)
            Helper.remove_file(file_path)
            logging.debug("File '" + file_path + "' is a mp4-file.")
        else:
            logging.debug("File '" + file_path + "' is not a mp4-file.")

        return mp4_file, size_mp4_file

    @staticmethod
    def convert_bytes_to_mega_bytes(bytes_size) -> (int, int):
        """
        The method converts the number bytes from the input into megabytes and
        medibytes
        :param bytes_size: The bytes number to convert
        :return: The converted number in megabytes and medibytes
        """

        bytes_in_megabytes = 1000000.0
        bytes_in_mebibytes = 1048576.0

        megabytes_size = bytes_size / bytes_in_megabytes
        megabytes_size = round(megabytes_size, 2)

        mebibytes_size = bytes_size / bytes_in_mebibytes
        mebibytes_size = round(mebibytes_size, 2)

        return megabytes_size, mebibytes_size
