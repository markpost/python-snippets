# Remover for mp4-file

This snippet is a remover of all mp4-files from all directories and subdirectories of the given 
path. The main.py is the entry point of the snippet.

## Usage
The script uses a single argument to run: the path of the directories and subdirectories to check 
for mp4-files. 

```
python3 main.py [path]
```

## Requirements
This snippet requires Python 3.

## Changes
* 2018-12-23 - Initial version