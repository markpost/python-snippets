#!/usr/bin/env python
"""
The class contains a number of helper classes for the remover class
"""

import logging
import os


class Helper:

    def __init__(self):
        pass

    @staticmethod
    def remove_file(file_path):
        """
        The method is a wrapper method for removing files
        :param file_path: The path of the file to remove
        """
        os.remove(file_path)

    @staticmethod
    def get_file_size(file_path) -> int:
        """
        The method is a wrapper method to retreive to file size of the given file
        :param file_path: The file path of the file to get the size of
        :return: The file size of the file
        """
        return os.path.getsize(file_path)

    @staticmethod
    def is_file_mp4(file_name) -> bool:
        """
        Method checks whether file is a mp4-file by checking its' extension
        :param file_name: The path of the file to check
        :return: Whether or not the file a mp4-file is
        """
        return file_name.endswith(".mp4")

    @staticmethod
    def is_path_dir(dir_path) -> bool:
        """
        The method is a wrapper method to check whether the given path is a path to
        a directory
        :param dir_path: The path of the directory to check
        :return: Whether or not the path is a directory
        """
        return os.path.isdir(dir_path)

    @staticmethod
    def is_path_given(arguments) -> bool:
        """
        The method checks whether the arguments list contains a valid path
        :param arguments: The arguments list to check
        :return: Whether or not the arguments list contains a valid path
        """
        if 0 == len(arguments):
            logging.error(
                "No path given in the arguments. Please give a path and run again.")
            return False

        path = arguments[1]
        path = os.path.normpath(path)

        if not Helper.is_path_dir(path):
            logging.error("Given path is invalid or not a path of a directory.")
            return False

        return True
