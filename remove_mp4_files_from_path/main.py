#!/usr/bin/env python

import logging
import sys

from remover import Remover

logging.basicConfig(level=logging.INFO)


def main():
    """
    The main method and entry point for the remover of mp4-files from the path
    given as argument.

    Run as: main.py [path]
    """
    remover = Remover()
    arguments = sys.argv

    remover.process(arguments)


main()
