# Python snippets

Personal repository with Python snippets. Please see the README of the snippet for more information 
about the snippet and it workings.

## Snippets overview
* remove_mp4_files_from_path - Snippet that removes all mp4-files from the given path.

## License
See LICENSE file.